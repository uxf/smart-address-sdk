<?php

declare(strict_types=1);

namespace SmartAddressTests;

use SmartAddress\SmartAddressService;

final readonly class SmokeServiceProvider
{
    public function __construct(
        public SmartAddressService $service,
    ) {
    }
}
