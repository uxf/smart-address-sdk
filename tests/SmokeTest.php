<?php

declare(strict_types=1);

namespace SmartAddressTests;

use PHPUnit\Framework\Attributes\RequiresPhp;
use SmartAddress\Http\Request\SearchQueryRequest;
use SmartAddress\Http\Request\SearchZipRequestQuery;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

#[RequiresPhp('666')]
class SmokeTest extends KernelTestCase
{
    public function test(): void
    {
        $provider = self::getContainer()->get(SmokeServiceProvider::class);
        assert($provider instanceof SmokeServiceProvider);
        $service = $provider->service;

        $address = $service->getAddress(23713011);
        self::assertSame(23713011, $address->id);

        $address = $service->getExtendedAddress(23713011);
        self::assertSame(23713011, $address->zakladniAdresa->id);

        $addresses = $service->search(new SearchQueryRequest(term: 'Sovova 584'));
        self::assertSame(23713011, $addresses[0]->id);

        $addresses = $service->searchByZip(new SearchZipRequestQuery('46014'));
        self::assertNotEmpty($addresses);
    }

    protected static function getKernelClass(): string
    {
        return Kernel::class;
    }
}
