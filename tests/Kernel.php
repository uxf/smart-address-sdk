<?php

declare(strict_types=1);

namespace SmartAddressTests;

use Psr\Http\Message\RequestFactoryInterface;
use SmartAddress\SmartAddressBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use UXF\Core\UXFCoreBundle;
use UXF\Hydrator\UXFHydratorBundle;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    public function registerBundles(): iterable
    {
        yield new FrameworkBundle();
        yield new UXFCoreBundle();
        yield new UXFHydratorBundle();
        yield new SmartAddressBundle();
    }

    protected function configureContainer(ContainerConfigurator $container): void
    {
        $container->extension('framework', [
            'test' => true,
        ]);

        $container->extension('smart_address', [
            'base_url' => 'https://smart-address.uxf.dev',
        ]);

        $services = $container->services();
        $services->set(SmokeServiceProvider::class)->autowire()->public();

        $services->alias(RequestFactoryInterface::class, 'psr18.http_client');
    }
}
