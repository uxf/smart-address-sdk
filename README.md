# Smart Address SDK

## Install
```
$ composer req uxf/smart-address-sdk
```

```php
// bundles.php
return [
    UXFCoreBundle::class => ['all' => true];
    UXFHydratorBundle::class => ['all' => true];
    SmartAddressBundle::class => ['all' => true];
];
```

## Config (optional)

```php
// packages/smart_address.php
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function (ContainerConfigurator $containerConfigurator): void {
    $containerConfigurator->extension('smart_address', [
        'base_url' => 'https://www.smart-address.cz', // default value
    ]);
};
```

## Usage

```php
use SmartAddress\SmartAddressService;
use SmartAddress\Http\Request\SearchQueryRequest;
use SmartAddress\Http\Request\SearchZipRequestQuery;

class LocationService
{
    public function __construct(private readonly SmartAddressService $service)
    {
    } 

    public function run()
    {
        $address = $service->getAddress(23713011);
        $address = $service->getExtendedAddress(23713011);
        $addresses = $service->search(new SearchQueryRequest(term: 'Sovova 584'));
        $addresses = $service->searchByZip(new SearchZipRequestQuery('46014'));
    }
}
```
