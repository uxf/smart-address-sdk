<?php

declare(strict_types=1);

namespace SmartAddress\Exception;

use Exception;
use Throwable;

final class SmartAddressException extends Exception
{
    public function __construct(
        string $errorMessage,
        ?Throwable $previous = null,
    ) {
        parent::__construct($errorMessage, 0, $previous);
    }
}
