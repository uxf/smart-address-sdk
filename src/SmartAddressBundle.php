<?php

declare(strict_types=1);

namespace SmartAddress;

use SmartAddress\Client\HttpClient;
use SmartAddress\Client\HttpSmartAddressService;
use Symfony\Component\Config\Definition\Configurator\DefinitionConfigurator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

class SmartAddressBundle extends AbstractBundle
{
    protected string $extensionAlias = 'smart_address';

    public function configure(DefinitionConfigurator $definition): void
    {
        $definition->rootNode()
            ->children()
            ->scalarNode('base_url')->defaultValue('https://www.smart-address.cz')->end()
            ->end()
            ->end();
    }

    /**
     * @param array<mixed> $config
     */
    public function loadExtension(array $config, ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $services = $container->services();

        $services->set(SmartAddressService::class, HttpSmartAddressService::class)
            ->autowire();

        $services->set(HttpClient::class)
            ->arg('$baseUrl', $config['base_url'])
            ->autowire();
    }
}
