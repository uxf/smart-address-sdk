<?php

declare(strict_types=1);

namespace SmartAddress\Client;

use SmartAddress\Exception\IncorrectStructureException;
use SmartAddress\Http\Request\SearchQueryRequest;
use SmartAddress\Http\Request\SearchZipRequestQuery;
use SmartAddress\Http\Response\AddressExtendedResponse;
use SmartAddress\Http\Response\BasicAddressResponse;
use SmartAddress\SmartAddressService;
use UXF\Hydrator\Exception\HydratorException;
use UXF\Hydrator\ObjectHydrator;

final readonly class HttpSmartAddressService implements SmartAddressService
{
    public function __construct(
        public ObjectHydrator $objectHydrator,
        public HttpClient $httpClient,
    ) {
    }

    public function getAddress(int $id): BasicAddressResponse
    {
        try {
            $address = $this->httpClient->get("/api/v2/address/{$id}");

            return $this->objectHydrator->hydrateArray($address, BasicAddressResponse::class);
        } catch (HydratorException $e) {
            throw new IncorrectStructureException($e->getMessage(), $e);
        }
    }

    public function getExtendedAddress(int $id): AddressExtendedResponse
    {
        try {
            $extended = $this->httpClient->get("/api/v2/address/{$id}/extended");

            return $this->objectHydrator->hydrateArray($extended, AddressExtendedResponse::class);
        } catch (HydratorException $e) {
            throw new IncorrectStructureException($e->getMessage(), $e);
        }
    }

    public function search(SearchQueryRequest $query): array
    {
        try {
            $adressess = $this->httpClient->get('/api/v2/search?' . http_build_query($query));

            return $this->objectHydrator->hydrateArrays($adressess, BasicAddressResponse::class);
        } catch (HydratorException $e) {
            throw new IncorrectStructureException($e->getMessage(), $e);
        }
    }

    public function searchByZip(SearchZipRequestQuery $query): array
    {
        try {
            $adressess = $this->httpClient->get('/api/v2/zip/search?' . http_build_query($query));

            return $this->objectHydrator->hydrateArrays($adressess, BasicAddressResponse::class);
        } catch (HydratorException $e) {
            throw new IncorrectStructureException($e->getMessage(), $e);
        }
    }
}
