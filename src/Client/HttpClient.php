<?php

declare(strict_types=1);

namespace SmartAddress\Client;

use Nette\Utils\Json;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use SmartAddress\Exception\SmartAddressException;

final class HttpClient
{
    /** @var array<string, mixed> */
    private array $cache = [];

    public function __construct(
        private readonly string $baseUrl,
        private readonly ClientInterface $httpClient,
        private readonly RequestFactoryInterface $requestFactoryInterface,
    ) {
    }

    /**
     * @return mixed[]
     */
    public function get(string $url, bool $useCache = true): array
    {
        if ($useCache && isset($this->cache[$url])) {
            return $this->cache[$url];
        }

        $request = $this->requestFactoryInterface->createRequest('GET', $this->baseUrl . $url);
        $response = $this->httpClient->sendRequest($request);
        $responseBody = $response->getBody()->getContents();

        if ($response->getStatusCode() !== 200) {
            throw new SmartAddressException($responseBody);
        }

        return $this->cache[$url] = Json::decode($responseBody, true);
    }
}
