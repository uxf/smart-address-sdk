<?php

declare(strict_types=1);

namespace SmartAddress;

use SmartAddress\Http\Request\SearchQueryRequest;
use SmartAddress\Http\Request\SearchZipRequestQuery;
use SmartAddress\Http\Response\AddressExtendedResponse;
use SmartAddress\Http\Response\BasicAddressResponse;

interface SmartAddressService
{
    public function getAddress(int $id): BasicAddressResponse;

    public function getExtendedAddress(int $id): AddressExtendedResponse;

    /**
     * @return BasicAddressResponse[]
     */
    public function search(SearchQueryRequest $query): array;

    /**
     * @return BasicAddressResponse[]
     */
    public function searchByZip(SearchZipRequestQuery $query): array;
}
