<?php

declare(strict_types=1);

namespace SmartAddress\Http\Request;

use Symfony\Component\Validator\Constraints as Assert;

final readonly class SearchCityPartRequestQuery
{
    public function __construct(
        #[Assert\Regex(pattern: '/^[\d]{0,5}$/', message: 'Zadejte 0 - 5 číslic')]
        public string $zip = '',
        #[Assert\LessThanOrEqual(value: 200)]
        public int $limit = 50,
    ) {
    }
}
