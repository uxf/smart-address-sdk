<?php

declare(strict_types=1);

namespace SmartAddress\Http\Request;

use Symfony\Component\Validator\Constraints as Assert;

final readonly class SearchQueryRequest
{
    public function __construct(
        public string $term = '',
        #[Assert\Range(min: 1, max: 6)]
        public int $level = 6,
        #[Assert\LessThanOrEqual(value: 100)]
        public int $limit = 10,
        public ?int $zip = null,
        public ?string $address = null,
    ) {
    }
}
