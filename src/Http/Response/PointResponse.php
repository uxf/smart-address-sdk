<?php

declare(strict_types=1);

namespace SmartAddress\Http\Response;

final readonly class PointResponse
{
    public function __construct(
        public float $latitude,
        public float $longitude,
    ) {
    }
}
