<?php

declare(strict_types=1);

namespace SmartAddress\Http\Response;

use UXF\Core\Type\Date;

final readonly class BasicAddressResponse
{
    public function __construct(
        public int $id,
        public int $obecKod,
        public ?string $obecNazev,
        public ?string $obec,
        public ?int $momcKod,
        public ?string $momcNazev,
        public ?int $mopKod,
        public ?string $mopNazev,
        public ?int $castObceKod,
        public ?string $castObceNazev,
        public ?int $uliceKod,
        public ?string $uliceNazev,
        public ?string $ulice,
        public ?string $typSo,
        public ?int $cisloDomovni,
        public ?int $cisloOrientacni,
        public ?string $cisloOrientacniZnak,
        public ?int $psc,
        public ?string $search,
        public ?int $level,
        public ?PointResponse $gps,
        public ?Date $platiOd,
        public ?Date $aktualizace,
        public ?int $okresKod,
        public ?string $okresNazev,
        public ?int $krajKod,
        public ?string $krajNazev,
    ) {
    }
}
