<?php

declare(strict_types=1);

namespace SmartAddress\Http\Response;

final readonly class AddressExtendedBuildingResponse
{
    public function __construct(
        public int $id,
        public ?BuildingTypeResponse $typBudovy,
        public ?ConstructionTypeResponse $druhKonstrukce,
        public ?int $pocetBytu,
        public ?int $pocetPodlazi,
        public ?SewerConnectionTypeResponse $pripojeniKanalizace,
        public ?WaterConnectionTypeResponse $pripojeniVodovod,
        public ?ElevatorTypeResponse $vybaveniVytahem,
        public ?int $zastavenaPlocha,
        public ?int $obestavenyProstor,
        public ?int $podlahovaPlocha,
        public ?string $objectSize,
    ) {
    }
}
