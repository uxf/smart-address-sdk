<?php

declare(strict_types=1);

namespace SmartAddress\Http\Response;

final readonly class AddressExtendedEnergyResponse
{
    public function __construct(
        public int $id,
        public ?string $distributorElektrinyKod,
        public ?int $distributorElektrinyId,
        public ?string $distributorPlynuKod,
        public ?int $distributorPlynuId,
        public ?GasConnectionTypeResponse $pripojeniPlyn,
        public ?HeatingTypeResponse $zpusobVytapeni,
    ) {
    }
}
