<?php

declare(strict_types=1);

namespace SmartAddress\Http\Response;

final readonly class AddressExtendedAdministrativeResponse
{
    public function __construct(
        public int $id,
        public ?string $isknBudovaId,
        public ?string $stavebniObjektKod,
        public ?string $parcelaKod,
        public ?BuildingUsageTypeResponse $zpusobVyuzitiObjektu,
        public ?ObjectTypeBasicResponse $objectType,
        public ?ObjectTypeBasicResponse $objectTypeBasic,
    ) {
    }
}
