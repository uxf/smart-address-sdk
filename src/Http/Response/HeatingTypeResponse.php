<?php

declare(strict_types=1);

namespace SmartAddress\Http\Response;

use UXF\Core\Type\Date;

final readonly class HeatingTypeResponse
{
    public function __construct(
        public int $id,
        public ?string $nazev,
        public ?string $popis,
        public ?string $zkracenyNazev,
        public ?Date $zacatekPlatnosti,
        public ?Date $konecPlatnosti,
    ) {
    }
}
