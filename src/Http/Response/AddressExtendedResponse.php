<?php

declare(strict_types=1);

namespace SmartAddress\Http\Response;

final readonly class AddressExtendedResponse
{
    public function __construct(
        public BasicAddressResponse $zakladniAdresa,
        public ?AddressExtendedAdministrativeResponse $administrativniUdaje,
        public ?AddressExtendedBuildingResponse $udajeBudovy,
        public ?AddressExtendedEnergyResponse $udajeEnergii,
    ) {
    }
}
