<?php

declare(strict_types=1);

namespace SmartAddress\Http\Response;

final readonly class ObjectTypeBasicResponse
{
    public function __construct(
        public int $id,
        public ?string $nazev,
    ) {
    }
}
